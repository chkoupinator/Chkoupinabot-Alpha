

async def execute(chkoupinabot, msg, args):
    appinfo = await chkoupinabot.application_info()
    if msg.author != appinfo.owner:
        await msg.channel.send("Only the bot owner can use this command.")
        return
    print(args)
    if args[0] == 'set':
        if args[1] == 'name':
            if args[2].startswith('"') and args[2].endswith('"'):
                name = args[2].replace('"', '')
            else:
                name = " ".join(args[2:])
            print('new name : '+name)
            await chkoupinabot.user.edit(username=name)
            await msg.channel.send("Name has been successfully set to "+name)
